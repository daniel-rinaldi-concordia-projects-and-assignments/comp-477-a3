#include "a3solution.h"

#include "dependencies/Eigen/Dense"
#include "QDebug"
#include "QElapsedTimer"

using Eigen::Vector2f;
using Eigen::Vector4f;
using Eigen::VectorXf;
using Eigen::MatrixXd;
using Eigen::MatrixXf;

A3Solution::A3Solution(
    std::vector<Joint2D*>& joints, std::vector<Spring2D*>& springs, float& gravity, float& positional_damping,
    float& mass, float& timestep, bool& implicit, float& stiffness
) : m_joints(joints), m_links(springs), m_gravity(gravity), m_positional_damping(positional_damping),
    m_mass(mass), m_timestep(timestep), m_implicit(implicit), m_stiffness(stiffness),
    free_joints(), vecx_Yk(), vecx_Yk1()
{
    num_joints = 0;
    last_update_time = time(0);
}

void A3Solution::update(Joint2D* selected, QVector2D mouse_pos)
{
    // track the joint being dragged
    mousedriven_joint = selected;

    selected->set_position(mouse_pos);
}

void A3Solution::update()
{
    // check if joints were added/removed or if it's been a while since the last time we updated
    if (num_joints != m_joints.size() || time(0) - last_update_time > 2)
    {
        // re-initialize the system
        initialize_system();
    }
    last_update_time = time(0);

    explicit_euler();

    if (m_implicit)
    {
        // do implicit euler (use result of explicit as initial guess)
        implicit_euler();
    }

    // iterate over free joints and update positions
    for (auto i = 0; i < free_joints.size(); ++i)
    {
        Joint2D* joint = free_joints[i];

        // ignore joint that's being dragged by mouse
        if (joint == mousedriven_joint)
        {
            // set position
            QVector2D qvec_mdjoint_position = mousedriven_joint->get_position();
            vecx_Yk[(i*4)] = qvec_mdjoint_position.x();
            vecx_Yk[(i*4)+1] = -qvec_mdjoint_position.y(); // coversion from/to Qt/Eigen
            // set velocity to 0
            vecx_Yk[(i*4)+2] = 0;
            vecx_Yk[(i*4)+3] = 0;
            continue;
        }

        joint->set_position(QVector2D(vecx_Yk[(i*4)+0], -vecx_Yk[(i*4)+1])); // coversion from/to Qt/Eigen
    }

    // reset mousedriven_joint
    mousedriven_joint = nullptr;
}

void A3Solution::initialize_system()
{
    // set total number of joints
    num_joints = m_joints.size();

    // init free_joints vector
    free_joints.clear();
    for (Joint2D* joint : m_joints)
    {
        if (!joint->is_locked())
            free_joints.push_back(joint);
    }

    // init Yk and Yk_prime
    int num_Ys = free_joints.size()*4;
    vecx_Yk = VectorXf(num_Ys);
    vecx_Yk_prime = VectorXf(num_Ys);
    for (int i = 0; i < num_Ys; i = i + 4)
    {
        Joint2D* joint = free_joints[i / 4];

        // set Yk position
        vecx_Yk[i] = joint->get_position().x();
        vecx_Yk[i+1] = -(joint->get_position().y()); // coversion from/to Qt/Eigen

        // set Yk velocity
        vecx_Yk[i+2] = 0;
        vecx_Yk[i+3] = 0;
    }
}

void A3Solution::explicit_euler()
{
    calculate_Yk_prime();

    // do time integration
    // Yk+1 = Yk + (Yk_prime * timestep)
    vecx_Yk = vecx_Yk + (vecx_Yk_prime * m_timestep);
}

void A3Solution::calculate_Yk_prime()
{
    // iterate over free joints and calculate forces and acceleration and update Yk_prime
    for (auto i = 0; i < free_joints.size(); ++i)
    {
        Joint2D* joint = free_joints[i];

        // ignore joint that's being dragged by mouse
        if (joint == mousedriven_joint) continue;

        // calculate force and acceleration
        Vector2f net_force = calculate_net_force(i);
        Vector2f acceleration = net_force / m_mass;

        // set Yk_prime velocity
        vecx_Yk_prime[(i*4)] = vecx_Yk[(i*4)+2];
        vecx_Yk_prime[(i*4)+1] = vecx_Yk[(i*4)+3];

        // set Yk_prime acceleration
        vecx_Yk_prime[(i*4)+2] = acceleration.x();
        vecx_Yk_prime[(i*4)+3] = acceleration.y();
    }
}

Vector2f A3Solution::calculate_net_force(int joint_index)
{
    Joint2D* joint = free_joints[joint_index];

    // calculate gravitational force acting on the joint
    // f_g = -g * m
    // g = gravity
    // m = mass
    Vector2f f_total = Vector2f(0, -m_gravity * m_mass);

    // calculate internal and damping forces acting on the joint
    std::vector<Spring2D*> adj_springs = joint->get_springs();
    for (auto i = 0; i < adj_springs.size(); ++i)
    {
        Joint2D* other_joint = adj_springs[i]->get_other_joint(joint);
        if (other_joint != nullptr)
        {
            Vector2f f_int_damp = Vector2f(0, 0);

            // add internal force
            // f_int = -k * (l - L) * (x - x0)/l
            // x0 = other joint, x = current joint
            // k = stiffness of the spring
            // l = ||x - x0|| = current length of the link
            // L = length of the link at rest
            QVector2D qvec_x_x0 = (joint->get_position() - other_joint->get_position());
            Vector2f x_x0 = Vector2f(qvec_x_x0.x(), -qvec_x_x0.y()); // coversion from/to Qt/Eigen
            float l = adj_springs[i]->get_length();
            float L = adj_springs[i]->get_rest_length();

            f_int_damp += -m_stiffness * (l - L) * x_x0.normalized();

            // add damping force
            // f_damp = -Y * v
            // Y = positional damping
            // v = velocity
            f_int_damp += -m_positional_damping * Vector2f(vecx_Yk[(joint_index*4)+2], vecx_Yk[(joint_index*4)+3]);

            // half the force if the other joint is not locked
            if (!other_joint->is_locked())
                f_int_damp /= 2;

            f_total += f_int_damp;
        }
    }

    return f_total;
}

VectorXf A3Solution::implicit_euler()
{
    VectorXf Zk = VectorXf();
      // TODO
//    while(i < max_iterations)
//    {
//        Zk1 = Zk + g(Zk)/g(Zkp);

//        Zk <-- (x0, x2, v0, v1);
//        Zk1 <-- (v0, v1, a0, a1);
//        Fk = get_forces(Zk);
//        Ak = Fk / m;
//        jacob = get_jacobian(Zk);

//        g(Zk) = Zk - Yn - Zkp * m_timestep;
//        gp(Zk) = I - 0 - jacob * m_timestep;

//        Zk = Zk1;
//    }

    return Zk;
}

//get_forces()
//{
//
//}

//MatrixXf get_jacobian(Yn)
//{
//    Case for 1 joint:
//    damp = m_positional_damping;
//    d(x,y) = derivitive of x w.r.t y
//    J = [          J0
//              v0    v1    a0       a1
//             --------------------------
//          x0| 0     0  d(a0,x0) d(a1,x0)
//      J0  x1| 0     0  d(a0,x1) d(a1,x1)
//          v0| 1     0  -damp/m     0
//          v1| 0     1     0     -damp/m
//    ]
//}

void A3Solution::test_eigen_library(){

    // create a simple matrix 5 by 6
    MatrixXd mat(5,6);

    // Fills in matrix
    // Important Note: Eigen matrices are row major
    // so mat(0,1) references the 0-th column and 1-th row
    for(unsigned int row=0;row<mat.rows();row++)
    {
        for(unsigned int col=0;col<mat.cols();col++)
        {
            mat(row,col) = row+col;
        }
    }

    // create the pseudoinverse
    MatrixXd pseudo_inv = mat.completeOrthogonalDecomposition().pseudoInverse();

    // print the pseudoinverse
    std::cout << "--------------------------" << std::endl;
    std::cout << pseudo_inv << std::endl;
    std::cout << "--------------------------" << std::endl;
}
