#ifndef A2SOLUTION_H
#define A2SOLUTION_H

#include <ctime>
#include <vector>
#include <algorithm>

#include "OpenGL/elements/joint2D.h"
#include "OpenGL/elements/spring2d.h"
#include "dependencies/Eigen/Dense"

using Eigen::Vector2f;
using Eigen::VectorXf;
using Eigen::MatrixXf;

class A3Solution
{
public:
    A3Solution(std::vector<Joint2D*>& joints, std::vector<Spring2D*>& springs, float& gravity, float& positional_damping, float& mass, float& timestep, bool& implicit, float& stiffness);

    // OpenGL members (these are updated for you)
    std::vector<Joint2D*>& m_joints;
    std::vector<Spring2D*>& m_links;
    float& m_gravity;
    float& m_positional_damping;
    float& m_mass;
    float& m_timestep;
    bool& m_implicit;
    float& m_stiffness;

    // Separate tracking of positions and velocities
    void update();
    void update(Joint2D* selected, QVector2D mouse_pos);

    static void test_eigen_library();

private:
    Joint2D* mousedriven_joint;
    int num_joints;
    time_t last_update_time;
    std::vector<Joint2D*> free_joints;
    VectorXf vecx_Yk;
    VectorXf vecx_Yk_prime;
    VectorXf vecx_Yk1;

    void initialize_system();
    void explicit_euler();
    void calculate_Yk_prime();
    Vector2f calculate_net_force(int joint_index);
    VectorXf implicit_euler();
};

#endif // A2SOLUTION_H
